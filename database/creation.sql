-- Script de création de la base de données
-- Création de la base de données
CREATE DATABASE clublambda;

-- Utilisons cette nouvelle base de données


-- Création d'un utilisateur admin 
-- et on lui donne tous les droits sur la base de données
-- et sur toutes les tables de la base
CREATE USER 'adminClub'@'%' IDENTIFIED BY 'iY7p]67D*v';
GRANT ALL PRIVILEGES ON clublambda.* TO 'adminClub'@'%';

